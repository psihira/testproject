<?php

class Main extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $data['pageTitle'] = 'Main page';
        $this->render('main/index', $data);
    }
}