<?php

class Table extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('cats');
    }

    public function index()
    {
        $data['result'] = $this->cats->getAllData();
        $data['pageTitle'] = 'Table page';
        $this->render('table/index', $data);
    }
}