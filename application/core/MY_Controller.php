<?php

class MY_Controller extends MX_Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function render($view, $data)
    {
        $this->template->load($view, $data);
    }
}