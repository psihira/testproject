<?php

class MY_Model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    public function getTotal($tableName)
    {
        return $this->db->count_all($tableName);
    }

}