<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Template
{
    public function load($view = 'main' , $viewData = [], $return = FALSE)
    {
        $this->CI =& get_instance();
        $this->CI->load->view('templates/header');
        $content = $this->CI->load->view($view, $viewData, TRUE);
        $this->CI->load->view('templates/layout', ['content' => $content]);
        $this->CI->load->view('templates/footer');
    }
}
