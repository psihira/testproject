<!DOCTYPE html>
    <html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel = "stylesheet" type = "text/css" href = "<?= base_url(); ?>assets/bootstrap/css/bootstrap.min.css">
        <link rel = "stylesheet" type = "text/css" href = "<?= base_url(); ?>assets/bootstrap/css/bootstrap-theme.min.css">
        <link rel = "stylesheet" type = "text/css" href = "<?= base_url(); ?>assets/css/style.css">
        <script type = 'text/javascript' src = "<?= base_url(); ?>assets/js/jquery-3.2.1.js"></script>
        <script type = 'text/javascript' src = "<?= base_url(); ?>assets/bootstrap/js/bootstrap.min.js"></script>
