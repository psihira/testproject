<title><?= $pageTitle ?></title>
</head>
<body>
<header>
    <?php $this->load->view('templates/menu');?>
</header>
<div class="container" id="main-container">
    <div class="wrap">
        <?= $content ?>
    </div>
</div>