<div class="wrap">
    <div class="row">
       <div class="col-md-12">
                <h3>Список котов</h3>
       </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="table-responsive">
                <table  class="table table-bordered table-hover">
                    <thead>
                    <tr>
                        <th class="col-xs-1">№</th>
                        <th class="col-sm-2">Имя</th>
                        <th class="col-sm-2">Возраст</th>
                        <th class="col-sm-1">Порода</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach($result  as $row): ?>
                    <tr>
                        <td class="col-xs-1"><?= $row['id']; ?></td>
                        <td class="col-sm-2"><?= $row['name']; ?></td>
                        <td class="col-sm-2"><?= $row['age']; ?></td>
                        <td class="col-md-2"><?= $row['breed']; ?></td>
                    </tr>
                    <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
