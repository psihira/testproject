<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 29.11.2017
 * Time: 9:57
 */

class Cats extends MY_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function getAllData()
    {
        $query = $this->db->select('data.id as id, data.name as name, data.age as age, breed.breed_name as breed')
                 ->from('data')
                 ->join('breed', 'breed.id = data.breed_id', 'left')
                 ->get();
        return ($query->num_rows() > 0) ? $query->result_array() : [];
    }

}